import './styles.css';
import { Component } from 'react';
import { loadPost } from '../../utils/load-posts';
import { Posts } from '../../components/Posts';

class Home extends Component {
  state = {
      posts: [],
      allPosts: [],
      pages: 0,
      postsPerPage: 2
  };
 
  async componentDidMount() {
    await this.loadPost();
  }

  loadPost = async () => {
    const { pages, postsPerPage } = this.state;
    const postsAndPhotos = await loadPost();
    this.setState({ 
      posts: postsAndPhotos.slice(0,2), 
      allPosts: postsAndPhotos
    });
  }

  loadMorePosts = () => {

  }

  render() {
    const { posts } = this.state;

    return (
      <section className="container">
        <Posts posts={posts} />
        <button onClick={this.loadMorePosts}>Load more posts</button>
      </section>
    );
  }
} 

export default Home; 